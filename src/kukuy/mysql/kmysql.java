/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kukuy.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author urnusdev
 */
public class kmysql {

    private  Connection conexion;
    private String user;
    private String password;
    private String database;
    private String server;

    public kmysql() throws ClassNotFoundException, SQLException {
         Class.forName("com.mysql.cj.jdbc.Driver");
         
    }
    
    public kmysql connect() throws SQLException
    {
         conexion = DriverManager
                  .getConnection (
                          "jdbc:mysql://"+
                          this.getServer()+"/"+
                          this.getDatabase(),
                          this.getUser(), 
                          this.getPassword());
    return this;
    }

    public String getUser() {
        return user;
    }

    public kmysql setUser(String user) {
        this.user = user;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public kmysql setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getDatabase() {
        return database;
    }

    public kmysql setDatabase(String database) {
        this.database = database;
        return this;
    }

    public String getServer() {
        return server;
    }

    public kmysql setServer(String server) {
        this.server = server;
        return this;
    }

    public Connection getConexion() {
        return conexion;
    }
    
}
